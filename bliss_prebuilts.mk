include $(call first-makefiles-under,$(LOCAL_PATH))

PRODUCT_PACKAGES += \
     yetCalc \
     AboutBliss \
     Phonograph

ifeq ($(BLISS_BUILD_VARIANT),vanilla)
PRODUCT_PACKAGES += \
     Bromite \
     BromiteSystemWebView
endif
